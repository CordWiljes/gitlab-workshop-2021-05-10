# Forschungsprojekte managen mit GitLab

Datum: 10.05.2021, 13:00-17:00 Uhr

Herzlich willkommen zum Workshop "Forschungsprojekte managen mit GitLab"!

## Vorbereitung 

Zur Vorbereitung auf unseren Workshop stelle ich Ihnen Lernmaterialien zur Verfügung, die Sie dazu anleiten, Git auf Ihrem Rechner zu installieren, ein Projekt einzurichten und erste einfache Befehle auszuführen.

Sollten Sie während Ihrer online-Vorbereitung Fragen haben, können Sie diese als [Ticket einstellen](kontakt). Ich werde versuchen, diese dann zeitnah zu beantworten.


